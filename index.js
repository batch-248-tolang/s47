console.log("Hello Mars");
console.log(document);//reult - html document
console.log(document.querySelector("#txt-first-name"));
/*
	
	document refers to the whole web page
	querySelector is used to select a specific element (obj) as long as it is inside the html tag (HTML ELEMENT)
	-takes a string input that is formater like CSS selector
	-can select elements regardless if the string is an id, class, or a tag as long as the element is existing in the web page

	Alternative methofd that we use aside from querySelector in retrieving elements
	
	Syntax:
		document.getElementById()
		document.getElementByClassName()
		document.getElementByTagName()

*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// First Name only
txtFirstName.addEventListener("keyup", (event)=>{

	spanFullName.innerHTML = txtFirstName.value;

});


txtLastName.addEventListener("keyup", (event)=>{

	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
	
});

const labelFirstName = document.querySelector("#label-txt-first-name");

labelFirstName.addEventListener("click",(e)=>{

	alert("You clicked the First Name Label!")
})